<?php

namespace App\Console\Commands;

use Elasticsearch;
use Illuminate\Console\Command;

class IndexProducts extends Command
{
    /**
     * @var string
     */
    private const INDEX_NAME = 'products';

    /**
     * {@inheritdoc}
     */
    protected $signature = 'index:products';

    /**
     * {@inheritdoc }
     */
    protected $description = 'Index products to elasticsearch';

    /**
     * {@inheritdoc}
     */
    public function handle(): int
    {
        $hasIndex = Elasticsearch::indices()->exists([
            'index' => static::INDEX_NAME
        ]);
        if ($hasIndex) {
            Elasticsearch::indices()->delete([
                'index' => static::INDEX_NAME
            ]);
        }

        foreach ($this->getTestProduct() as $product) {
            Elasticsearch::index([
                'index' => static::INDEX_NAME,
                'id' => $product['id'],
                'body' => $this->prepareData($product),
            ]);
        }

        return static::SUCCESS;
    }

    /**
     * @param array $product
     * @return array
     */
    private function prepareData(array $product): array
    {
        return [
            'name' => $product['name'],
        ];
    }

    /**
     * @return array[]
     */
    private function getTestProduct(): array
    {
        return [
            [
                'id' => 1,
                'name' => 'Apple MacBook Air',
            ],
            [
                'id' => 2,
                'name' => 'Apple MacBook Pro',
            ],
            [
                'id' => 3,
                'name' => 'Apple Air Pods'
            ],
            [
                'id' => 4,
                'name' => 'Apple Watch',
            ],
            [
                'id' => 5,
                'name' => 'Apple iPhone'
            ],
            [
                'id' => 6,
                'name' => 'Apple iPhone Pro'
            ]
        ];
    }
}
