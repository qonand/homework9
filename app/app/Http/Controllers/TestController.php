<?php

namespace App\Http\Controllers;

use Elasticsearch;
use Illuminate\Http\Request;

class TestController extends Controller
{
    /**
     * @var string
     */
    private const INDEX_NAME = 'products';

    /**
     * @param Request $request
     *
     * @return array
     */
    public function run(Request $request): array
    {
        $search = $request->get('search');

        return [
            'status' => 'success',
            'data' => $search ? $this->searchProducts('MakBok') : [],
        ];
    }

    /**
     * @param string $search
     * @return array
     */
    private function searchProducts(string $search): array
    {
        $parameters = [
            'index' => static::INDEX_NAME,
            'body' => [
                'query' => [
                    'multi_match' => [
                        'query' => $search,
                        'fuzziness' => 3,
                        'fields' => ['name'],
                    ],
                ]
            ]
        ];

        $result = Elasticsearch::search($parameters);

        $products = [];
        foreach ($result['hits']['hits'] as $item) {
            $products[] = [
                'id' => $item['_id'],
                'name' => $item['_source']['name']
            ];
        }

        return $products;
    }
}
