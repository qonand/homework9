# How to install the application

1. Clone the repository `git clone https://qonand@bitbucket.org/qonand/homework9.git`
2. Run `docker-compose up -d nginx elasticsearch workspace` in `laradock` folder
3. Run `docker-compose exec workspace bash` in `laradock` folder and in opened bash run the following commands:

 - `composer install`
 - `php artisan key:generate`
 - `php artisan index:products`

# How to run the application
1. Run `docker-compose up -d nginx elasticsearch workspace` in `laradock` folder
2. Open `http://localhost/api/test/run?search={search}` in your web browser and enter search string

# Search example
GET http://localhost/api/test/run?search=MakBok

Result:
```
{
    "status": "success",
    "data": [
        {
            "id": "1",
            "name": "Apple MacBook Air"
        },
        {
            "id": "2",
            "name": "Apple MacBook Pro"
        }
    ]
}
```